package com.galvanize;

import java.io.*;
import java.util.ArrayList;
public class CellPhone extends CallingCard {

    private boolean activeCall = false;
    private ArrayList<String> callHistory = new ArrayList<>();
    private int callLength = 0;
    private CallingCard card;
    private boolean callCut = false;

    public CellPhone(CallingCard card){
        this.card = card;
    }

    public void call(String phoneNumber) {

        activeCall = true;
        callHistory.add(phoneNumber);
    }

    public void tick() {
        if (callLength < card.getRemainingMinutes()){
            callLength++;
        } else {
            endCall();
            callCut = true;
        }
    }

    public void endCall() {
        activeCall = false;
        if (callCut) {
            callHistory.add("Last call was cut at (" + callLength + " minutes) due to depleted minutes on calling card.");
        } else {
            if (callLength != 1) {
                callHistory.add("(" + callLength + " minutes)");
            } else {
                callHistory.add("(" + callLength + " minute)");
            }
        }
        card.useMinutes(callLength);
    }

    public ArrayList<String> getHistory(){

        return callHistory;
    }

}
