package com.galvanize;

public class CallingCard {

    private int centsPerMinute;
    private int minutes = 0;

    public CallingCard() {
        centsPerMinute = 0;
    }
    public CallingCard (int centsPerMinute) {
        this.centsPerMinute = centsPerMinute;
    }

    public void addDollars (int dollars) {
        int cents = dollars * 100;
        minutes = cents / centsPerMinute;
    }

    public int getRemainingMinutes() {
        if (minutes >= 0) {
            return minutes;
        } else {
            return 0;
        }
    }

    public void useMinutes(int usedMinutes) {
        minutes -= usedMinutes;
    }

}
